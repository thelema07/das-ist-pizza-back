const express = require("express");
const app = express();

const connectDB = require("./config/db");

connectDB();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,x-auth-token"
  );
  next();
});

app.get("/", (req, res) => {
  res.json({ msg: "This is a simple server!!!" });
});

app.use("/api/pizza", require("./routes/api/pizza-controller"));

module.exports = app;
