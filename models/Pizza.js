const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PizzaSchema = new Schema({
  name: { type: String, required: true },
  ingredients: { type: String, required: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  image: { type: String, required: true },
});

module.exports = Pizza = mongoose.model("pizza", PizzaSchema);
