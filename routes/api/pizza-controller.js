const express = require("express");
const router = express.Router();

const Pizza = require("../../models/Pizza");

// GET => method for fetch all avaiable Pizzas !!!
router.get("/all", async (req, res) => {
  try {
    const pizza = await Pizza.find();

    res.json(pizza);
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
});

// GET => method for getting a single pizza passing the id
router.get("/:id", async (req, res) => {
  try {
    const pizza = await Pizza.findById(req.params.id);

    if (!pizza) {
      return res.status(404).json({ msg: "Pizza not found :(" });
    }

    res.json(pizza);
  } catch (error) {
    console.error(error);
    if (error.kind === "ObjectId") {
      return res.status(404).json({ msg: "Post not found" });
    }
    res.status(500).send("Server Error");
  }
});

// POST => method for add new Pizza
router.post("/", async (req, res) => {
  // Implement Error Validation
  const { name, ingredients, description, price, image } = req.body;

  try {
    const pizza = new Pizza({ name, ingredients, description, price, image });

    await pizza.save();

    res.json(pizza);
  } catch (error) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const pizza = await Pizza.findByIdAndDelete(id);

    if (!pizza) {
      return res.status(404).json({ msg: "Pizza doesn't exists" });
    }

    res.status(200).json({ msg: "Pizza Deleted" });
  } catch (error) {
    console.error(error);
    if (error.kind === "ObjectId") {
      return res.status(404).json({ msg: "Pizza not found" });
    }
    res.status(500).send("Server Error");
  }
});

module.exports = router;
