const mongoose = require("mongoose");
const app = require("../app");
const request = require("supertest");

const mongoURI =
  "mongodb+srv://dannyHotCosaRica:dannyHotCosaRica@cluster0-xuksb.mongodb.net/test?retryWrites=true&w=majority";

// First, we establish connection with MongoDB database
beforeAll(async () => {
  await mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

afterAll(async () => {
  // await mongoose.connection.close();
  await mongoose.disconnect();
});

test("Server can get all Pizzas from API", async (done) => {
  await request(app).get("/api/pizza/all").expect(200);

  done();
});

test("Server can get a single pizza by id from API", async (done) => {
  await request(app).get("/api/pizza/5eadc96232ec29443000405d").expect(200);

  done();
});

test("An invalid call to API post will produce a 404", async (done) => {
  await request(app)
    .post("")
    .send({
      name: "Pizza Test",
      ingredients: "Jest,Supertest,Express",
      description: "A single mock call to test my code",
      price: 0.75,
      image: "assets/img/???.jpg",
    })
    .expect(404);

  done();
});

test("Server can post a single pizza to API", async (done) => {
  await request(app)
    .post("/api/pizza")
    .send({
      name: "Pizza Test",
      ingredients: "Jest,Supertest,Express",
      description: "A single mock call to test my code",
      price: 0.75,
      image: "assets/img/???.jpg",
    })
    .expect(200);

  done();
});
