# README

Thelema Cuisine - NodeJS Application

### Specifications

- Quick summary

App made with testing purposes, using NodeJS, ExpressJS and MongoDB as backend solution, using Jest and SuperTest for testing , using a single API Call, a customer can order, choose and navigate to order pizza, with 8 given options, the user can choose, add and remove any item he wants, and went he's ready, he can proceed to a checkout order.

- Version

  1.0.0

### Technology Stack

This app was created using

- NodeJS
- ExpressJS
- MongoDB
- Mongoose(ORM)
- Jest
- Supertest

### Contribution guidelines

- Download the repo
- Go to console and run "npm install"
- Go to console and run "node server.js" or "nodemon server.js"
- You can test the api endpoints using Postman or Swagger
- To run test go to console and run "npm run test"

### Owner

- Daniel Felipe Mesu
- danielrojo1927@gmail.com
